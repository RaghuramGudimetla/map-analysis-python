# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 10:22:43 2020
@author: Raghuram Gudimetla
"""

#Importing required libraries
import pandas as pd
import glob
import os
import matplotlib.pylab as plt
from pycountry_convert import country_name_to_country_alpha3
import plotly.io as pio
import plotly.express as px
pio.renderers.default='browser'

#File locations
path_tmin = r'P:/Scalable/All_Files' 
path_rainfall = r'P:/Scalable/rainfall_world_wide'

#files for tmin and tmax problems
files_tmin = glob.glob(os.path.join(path_tmin, "*.csv"))

#files for average rainfall problem
files_rainfall = glob.glob(os.path.join(path_rainfall, "*.csv"))

# Common functions -----------------------------------------------------------------------------

"""
    Function --- Appending the data of all files into a list
    @returns a data frame with data of all csv files
    @params csv files
"""
def append_files_data(all_files):
    
    #local variable to use as a list
    combined_files = []
    
    #Interagtion all the files
    for filename in all_files:
        
        try:

            df = pd.read_csv(filename) #file.csv is an empty csv file
            combined_files.append(df) #appending the data frame to the global list
            
        except pd.errors.EmptyDataError: #To catch exception if the file is empty
            print(filename + "This CSV file is empty")  
            
    combined_dataframe = pd.concat(combined_files, axis=0, ignore_index=True)
    
    return combined_dataframe

""" 
    Function -- To get the Alpha code of a country with country name
    @returns the country alpha code
    @params country name
"""
def get_country_alpha(country_name):
    
    alpha_code = ''
    
    try:
        alpha_code =  country_name_to_country_alpha3(country_name, cn_name_format="default")
    except:
        alpha_code = 'Unknown'
        
    return alpha_code

# Code related to tmin and tmax problem ------------------------------------------------------------------
            
#Getting the data for TMIN and TMAX problem        
tmin_tmax_df = append_files_data(files_tmin) 

#Converting string to date
tmin_tmax_df['DATE'] =  pd.to_datetime(tmin_tmax_df['DATE'], format='%Y%m%d')

#Extracting year from date variable
tmin_tmax_df['YEAR'] = pd.DatetimeIndex(tmin_tmax_df['DATE']).year 

#Rounding off the VALUE number
tmin_tmax_df['VALUE'] = tmin_tmax_df['VALUE'].div(10)

#Plots for each station of New Zealand
for key, grp in tmin_tmax_df.groupby(['NAME']): 
    grp = grp.pivot(index='DATE', columns='ELEMENT', values='VALUE') 
    grp.plot()
    plt.title(key)
    plt.show()
    
# Calculating average of TMIN and TMAX for whole data throughout years
average_df = tmin_tmax_df.groupby(['YEAR', 'ELEMENT']).mean().reset_index()

#Plotting of the average data overall
plt_avg = average_df.pivot(index='YEAR', columns='ELEMENT', values='VALUE')
plt_avg.plot()
plt.title("Average TMIN and TMAX accross years over the country")
plt.show()

# Code related to average rainfall problem ------------------------------------------------------------------

# Getting data for rainfall plotting
rainfall_df = append_files_data(files_rainfall) 

#Calculate average
average_rainfall_df = rainfall_df.groupby(['COUNTRY_NAME']).mean().reset_index()

#Adding a additional field for country code
average_rainfall_df['COUNTRY_CODE'] = average_rainfall_df['COUNTRY_NAME'].apply(get_country_alpha)

#removing countries with name unknown
average_rainfall_df = average_rainfall_df[average_rainfall_df['COUNTRY_CODE'] != 'Unknown']

#creatinga map to plot countries and their rainfall as a heat map
fig = px.choropleth(average_rainfall_df, locations="COUNTRY_CODE",
                    color="AVG_RAINFALL", # Avg_Rainfall is a column that used to color the heatmap
                    hover_name="COUNTRY_NAME", # column to add to hover information
                    color_continuous_scale=px.colors.sequential.YlGnBu)
fig.show()